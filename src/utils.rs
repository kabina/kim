use chrono::{Local, NaiveDateTime};

pub fn get_elapsed(val: NaiveDateTime) -> i64 {
    let now = Local::now().naive_local();
    return (now - val).num_seconds();
}

pub fn get_elapsed_opt(val: Option<NaiveDateTime>) -> i64 {
    match val {
        Some(x) => { 
            let now = Local::now().naive_local();
            return (now - x).num_seconds();
        }
        None => -1
    }
}
