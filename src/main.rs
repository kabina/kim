// Kabina client simulator
// It simulates cabs and customers without RestAPI, direct DB access
// Copyright (c) 2023 by Bogusz Jelinski bogusz.jelinski@gmail.com

mod model;
mod utils;
mod repo;
mod distance;
use std::{env, thread};
use std::collections::HashMap;
use chrono::Local;
use mysql::*;
use mysql::prelude::*;
use rand::Rng;
use rand::rngs::ThreadRng;
use log::{info,warn,LevelFilter};
use log4rs::{
    append::{
        console::{ConsoleAppender, Target},
        file::FileAppender,
    },
    config::{Appender, Config, Root},
    encode::pattern::PatternEncoder,
    filter::threshold::ThresholdFilter,
};
use model::{Order, OrderStatus, BusStatus, Leg, Bus, RouteStatus, CabStatus, Stop};
use utils::{get_elapsed, get_elapsed_opt};
use std::fs::File;
use std::io::{Write, BufReader, BufRead};
use std::str::FromStr;

#[derive(Clone)]
pub struct GenOrder {
    pub from: i32,
    pub to: i32,
}

const CFG_FILE_DEFAULT: &str = "kim.toml";
const CFG_ENTRIES: &'static [&'static str] = &[
    "db_conn",
    "cab_speed",
    "max_time", // duration of simulation
    "check_interval", // every how many senconds should the main loop execute
    "max_cab", // how many cabs are available 
    "max_stand", // number of stands/stops
    "stop_wait", // how long should a cab wait at the stop
    "req_per_min", // how many trips do customers request per min 
    "max_wait_for_assign", // the patience of a customer, how log can he put up with no answer from dispatcher
    "max_delay", // as with assignement, what is the acceptable delay beyond max_wait during pickup 
    "max_trip", // how long can a trip be, dispatcher's requirement
    "max_wait", // how long a customer want to wait for a cab
    "max_trip_actual", // 
    "max_trip_delay", // used to cancell trips that spanned much beyond trip duration + detour
    "max_detour" // max customers's loss in a pool, send together with max_wait in a request
];

fn main() -> std::result::Result<(), Box<dyn std::error::Error>> {
    let mut cfg_file: String = CFG_FILE_DEFAULT.to_string();

    // command line arguments
    let args: Vec<String> = env::args().collect();
    if args.len() == 3 && args[1] == "-f" {
        cfg_file = args[2].to_string();
    }
    info!("Config file: {cfg_file}");
    let cfg_src = read_cfg_file(cfg_file);
    let cfg: HashMap<String, u16> = get_cfg(&cfg_src);

    let db_conn_str = &cfg_src["db_conn"];
    info!("Database: {db_conn_str}");
    setup_logger(cfg_src["log_file"].clone());
    // init DB
    // 192.168.10.176
    let url: &str = db_conn_str;
    let pool = Pool::new(url)?;
    let mut client = pool.get_conn()?;
    let stops: Vec<Stop> = repo::read_stops(&mut client);
    info!("Stops: {}", stops.len());
    let taxi_orders: Vec<GenOrder>;

    distance::init_distance(&stops, cfg["cab_speed"]);

    if args.len() == 2 && args[1] == "gen" {
        gen_orders(cfg_src["orders_file"].clone(), cfg["req_per_min"], 
                    cfg["max_stand"], cfg["max_trip"], 1000);
        return Ok(());
    } else {
        taxi_orders = read_orders(cfg_src["orders_file"].clone());
    }

    let mut cabs: Vec<Bus> = init_cabs(&mut client, cfg["max_cab"], cfg["max_stand"]);
    let mut orders: Vec<Order> = vec![];
    let mut prev_elapsed: i64 = 0;
    let mut cust_id: i64 = 0;
    let loop_started = Local::now().naive_local();
    loop {
        let iter_started = Local::now().naive_local();
        let mut sql: String = "".to_string();
        sql += &check_cabs(&mut client, &mut cabs, cfg["stop_wait"],
                                             cfg["check_interval"]);
        let check_cabs_elapsed = get_elapsed(iter_started);
        sql += &check_orders(&mut client, &mut orders, &cabs, &cfg);
        let check_orders_elapsed = get_elapsed(iter_started);
        if get_elapsed(loop_started) < cfg["max_time"] as i64 * 60 { // STOP generating after max_time
            sql += &gen_new_orders(&mut cust_id, &taxi_orders, &cfg, prev_elapsed);
        }
        orders = clean_orders(orders); // remove CANCELLED, COMPLETED
        if sql.len() > 0 {
            match client.query_iter(&sql) {
                Ok(_) => {}
                Err(err) => {
                    warn!("SQL failed to run, err: {}", err);
                }
            }
        }
        prev_elapsed = get_elapsed(iter_started); // so that we can adjust number of customers generated per iteration
        info!("ELAPSED: Iteration took {}s, cabs: {}s, orders:{}s", prev_elapsed, check_cabs_elapsed, check_orders_elapsed);
        sleep(cfg["check_interval"]);
    }
}

fn read_orders(path: String) -> Vec<GenOrder> {
    let input = File::open(path).unwrap();
    let buffered = BufReader::new(input);
    let mut ret: Vec<GenOrder> = Vec::new();
    for line in buffered.lines() {
        match line {
            Ok(l) => {
                let parts = l.split(",");
                let collection = parts.collect::<Vec<&str>>();
                let from = i32::from_str(collection[0]).unwrap();
                let to = i32::from_str(collection[1]).unwrap();
                ret.push(GenOrder{from, to});
            }
            Err(_) => {}
        }
    }
    return ret;
}

fn gen_orders(path: String, req_per_min: u16, max_stand: u16, max_trip: u16, tries: usize) { // , rnd: &mut ThreadRng
    let mut i = 0;
    let mut rnd: ThreadRng = rand::thread_rng();
    let mut output = File::create(path.clone()).unwrap();
    info!("File:{}, req_per_min: {}", path, req_per_min);
    let mut orders: Vec<GenOrder> = Vec::new();
    while i < req_per_min as i32 * 60 {
        let (from, to) = rand_to_from(&mut rnd, max_stand, max_trip, tries);
        if from == -1 {
            continue;
        }
        let fresh_orders: Vec<GenOrder> = orders.clone()
            .into_iter()
            .filter(|r| r.from == from && r.to == to)
            .collect();
        if fresh_orders.len() == 0 { // avoid duplicates
            write!(output, "{},{}\n", from, to).unwrap();
            orders.push(GenOrder{from, to});
            i += 1;
        }
    }
    output.flush().unwrap();
}

fn sleep(secs: u16) {
    thread::sleep(std::time::Duration::from_secs(secs as u64));
}

fn clean_orders(orders: Vec<Order>) -> Vec<Order> {
    let mut new_orders: Vec<Order> = vec![];
    for o in orders {
        match o.status {
            OrderStatus::RECEIVED | OrderStatus::ASSIGNED | OrderStatus::PICKEDUP => {
                new_orders.push(o);
            },
            _  => {}
        }
    }
    return new_orders;
}

fn gen_new_orders(cust_id: &mut i64, orders: &Vec<GenOrder>, cfg: &HashMap<String, u16>, elapsed: i64) -> String {
    let mut sql: String = String::from("");
    let n: u16 = (cfg["req_per_min"] as f32 * ((cfg["check_interval"] as f32 + elapsed as f32)/60.0)) as u16;
    if *cust_id as usize >= (&orders).len() {
        return sql; // no more demand
    }
    for _ in 0..n {
        let o = &orders[*cust_id as usize];
        sql += &format!("INSERT INTO taxi_order (from_stand, to_stand, max_loss, max_wait, shared, in_pool, eta,\
                    status, received, distance, customer_id) VALUES ({},{},{},{},true,false,-1,0,now(),{},{});",
                    o.from, o.to, cfg["max_detour"], cfg["max_wait"],
                    unsafe { distance::DIST[o.from as usize][o.to as usize]}, cust_id);
        info!("Request cust_id={}, from={} to={}", cust_id, o.from, o.to);
        *cust_id += 1;
    }
    return sql;
}

fn rand_to_from(rnd: &mut ThreadRng, max_stand: u16, max_trip: u16, tries: usize) -> (i32, i32) {
    // let us find a trip that <= max_time
    for _ in 0..tries { // tenth time is a charm
        let from: i16 = (*rnd).gen_range(0..max_stand) as i16;
        let to: i16 = (*rnd).gen_range(0..max_stand) as i16;
        if from == to { continue; }
        if unsafe {distance::DIST[from as usize][to as usize]} <= max_trip as i16 {
            return (from as i32, to as i32);
        }
    }
    return (-1, -1);
}

fn check_orders(client: &mut PooledConn, orders: &mut Vec<Order>, cabs: &Vec<Bus>, cfg: &HashMap<String, u16>) -> String {
    let mut sql: String = String::from("");

    let db_orders: Vec<Order> = repo::read_orders(client);
    let orders_cpy = &orders.clone();
    // update our internal represantion of orders with what we have got from DB
    // TODO: this loop takes most time, make it more efficient!
    for o in db_orders.clone()  {
        let mut idx: usize = usize::MAX;
        for (pos, order) in orders_cpy.iter().enumerate() {
            if order.id == o.id {
                idx = pos as usize;
                break;
            }
        }
        if idx != usize::MAX { // update local info: status, cab,
            orders[idx].in_pool = o.in_pool;
            orders[idx].status = o.status;
            orders[idx].cab_id = o.cab_id;
            orders[idx].eta = o.eta;
            orders[idx].received = o.received;
            orders[idx].started = o.started;
        } else if o.status != OrderStatus::REFUSED {
            orders.push(o); // it might even be ASSIGNED at this time! or picked up!!! as we INSERTed this record in previous iteration
        }
    }
    let mut ids3: String = String::from("");
    let mut ids7: String = String::from("");
    let mut ids8: String = String::from("");
    // check if not too late or a cab has come?
    for (pos, o) in orders.clone().iter().enumerate()  {
        let cab_id: i64 =  match o.cab_id {
            Some(x) => { x }
            None => -1 // unassigned
        };
        if cab_id ==-1  && (o.status == OrderStatus::ASSIGNED || o.status == OrderStatus::PICKEDUP) {
            warn!("Error - no cab_id for ASSIGNED or PICKEDUP");
            continue;
        }
        match o.status {
            OrderStatus::RECEIVED => {
                if get_elapsed_opt(o.received) > cfg["max_wait_for_assign"] as i64 * 60 {
                    info!("Never assigned, cust_id={}", o.cust_id);
                    orders[pos].status = OrderStatus::CANCELLED;
                    ids3 += &format!("{},", o.id);
                    continue;
                }
            },
            OrderStatus::ASSIGNED => {
                if o.assigned == None { // first time here; this timestamp is not available in DB now
                    orders[pos].assigned = Some(Local::now().naive_local());
                } 
                if cab_location(cab_id, &cabs) == o.from {
                    if get_elapsed_opt(o.assigned) > (cfg["max_wait"] as i64 + 1) * 60 { // +1: one min, come on!
                        info!("Cab came late, cust_id={}, order_id={}, cab_id={},", o.cust_id, o.id, cab_id);
                    }
                    if get_elapsed_opt(o.assigned) > (o.eta as i64 + 2) * 60 { // HARDCODE !!
                        info!("Cab exceeded ETA, cust_id={}, order_id={}, cab_id={}, assigned: {}, eta: {}, picked-up: {}", 
                                o.cust_id, o.id, cab_id, o.assigned.unwrap(), o.eta, Local::now().naive_local());
                    }
                    info!("Picked up, cust_id={}, order_id={}, cab_id={},", o.cust_id, o.id, cab_id);
                    orders[pos].status = OrderStatus::PICKEDUP; // teoretically not needed (status will be updated from DB) but it happend that it wasn't
                    ids7 += &format!("{},", o.id);
                    continue;
                } else if get_elapsed_opt(o.assigned) > cfg["max_wait"] as i64 * 60 
                        + cfg["max_delay"] as i64 * 60 {
                 // another topic - if we shouldn't count wait time from RECEIVED
                    orders[pos].status = OrderStatus::CANCELLED;
                    info!("Waited in vain, cust_id={}, order_id={}", o.cust_id, o.id);
                    ids3 += &format!("{},", o.id);
                    continue;
                }
            },
            OrderStatus::PICKEDUP  => {
                let max_duration: i64 = (o.dist as f32 * (1.0 + (o.loss as f32 / 100.0)) 
                                         + cfg["max_trip_delay"] as f32) as i64 + 1; // + 1 as rounding or stop wait
                if cab_location(cab_id, &cabs) == o.to {
                    orders[pos].status = OrderStatus::COMPLETED; // to be removed from the list
                    if get_elapsed_opt(o.started) > max_duration * 60 {
                        info!("Completed late, cust_id={}, order_id={}, dist without pool={}, elapsed: {}", 
                                    o.cust_id, o.id, cab_id, get_elapsed_opt(o.started));
                    }
                    info!("Completed, cust_id={}, order_id={}, cab_id={},", o.cust_id, o.id, cab_id);
                    ids8 += &format!("{},", o.id);
                    continue;
                } else if cab_location(cab_id, &cabs) == o.from && cab_status(cab_id, &cabs) == BusStatus::WAITING
                        && get_elapsed_opt(o.started) > 60 { // CYCLE, it happens in pool finder (bug)
                    info!("Started once again, cust_id={}, order_id={}",  o.cust_id, o.id);
                    if get_elapsed_opt(o.assigned) > (cfg["max_wait"] as i64 + 1) * 60 { // +1: one min, come on!
                        info!("Cab came late the second time, cust_id={}, order_id={}, cab_id={},", o.cust_id, o.id, cab_id);
                    }
                    ids7 += &format!("{},", o.id);
                } else if get_elapsed_opt(o.started) > (cfg["max_wait"] as i64 + max_duration) * 60 {
                    orders[pos].status = OrderStatus::CANCELLED;
                    info!("Destination not reached, cust_id={}, order_id={}, cab_id={}, dist without pool={}, max_duration={}, elapsed: {}", 
                                o.cust_id, o.id, cab_id, o.dist, max_duration, get_elapsed_opt(o.started));
                    ids3 += &format!("{},", o.id);
                    continue;
                }
            },
            OrderStatus::REFUSED | OrderStatus::CANCELLED | OrderStatus::ABANDONED | OrderStatus::COMPLETED => {} // ignore it for now
        }
    }
    // remove CANCELLED and others from internal representation
    if ids3.len() > 0 {
        ids3.pop(); // remove last comma
        sql += &format!("UPDATE taxi_order SET status=3 WHERE id IN ({});", ids3);
    }
    if ids7.len() > 0 {
        ids7.pop();
        sql += &format!("UPDATE taxi_order SET status=7, started=now() WHERE id IN ({});", ids7);
    }
    if ids8.len() > 0 {
        ids8.pop();
        sql += &format!("UPDATE taxi_order SET status=8, completed=now() WHERE id IN ({});", ids8);
    }
    return sql;
}


fn cab_location(cab_id: i64, cabs: &Vec<Bus>) -> i32 {
    for cab in cabs {
        if cab.cab == cab_id {
            return cab.location;
        }
    }
    warn!("Cab not found, cab_id={},", cab_id);
    return -1;
}

fn cab_status(cab_id: i64, cabs: &Vec<Bus>) -> BusStatus {
    for cab in cabs {
        if cab.cab == cab_id {
            return cab.status.clone();
        }
    }
    warn!("Cab not found, cab_id={},", cab_id);
    return BusStatus::UNASSIGNED;
}

fn check_cabs(client: &mut PooledConn, cabs: &mut Vec<Bus>, stop_wait: u16, check_interval: u16) -> String {
    let mut sql: String = String::from("");
    let routes: Vec<Leg> = repo::read_routes(client); // active routes

    if routes.len() == 0 {
        thread::sleep(std::time::Duration::from_secs(check_interval as u64));
        return sql;    
    }
    // update all cabs
    let mut prev_route = -1;
    
    for (pos, leg) in routes.iter().enumerate() {
        if leg.route_id != prev_route { // new route, we need the first leg only
            let is_last = pos == routes.len() - 1 || routes[pos + 1].route_id != leg.route_id;
            sql += &check_cab(leg.cab_id, cabs, *leg, stop_wait, is_last);
            prev_route = leg.route_id;
        }
    }
    return sql;
}

fn check_cab(cab_idx: i64, cabs: &mut Vec<Bus>, leg: Leg, stop_wait: u16, is_last: bool) -> String {
    let mut sql: String = String::from("");

    cabs[cab_idx as usize].leg = leg;
    
    let bus: Bus = cabs[cab_idx as usize].clone();
    let elapsed = get_elapsed(bus.started);
    if elapsed == -1 {
        warn!("Error - elapsed failed, cab_id={},", cab_idx);
        return sql;
    }
    let mut ids_started = String::from("");
    let mut ids_completed = String::from("");
    let mut r_ids_completed = String::from("");
    let mut r_ids_started = String::from("");
    match bus.status {
        BusStatus::UNASSIGNED => { // new route
            info!("New route to run, cab_id={}, route_id={},", cab_idx, leg.route_id);
            r_ids_started += &format!("{},", leg.route_id); 

            if bus.location != leg.from {
                warn!("Error - cab is not at the start of route, cab_id:{}, route_id: {}, location: {}", 
                        cab_idx, leg.route_id, bus.location);
                cabs[cab_idx as usize].location = leg.from;
            }
            if leg.passengers > 0 { // pick-up from the location, the first leg takes a passenger
                cabs[cab_idx as usize].status = BusStatus::WAITING;  // wait for the passenger, no DB update
                info!("Waiting at a stop: {}, cab_id={},", leg.from, cab_idx);
            } else {
                // otherwise go pickup the first one to the next stop
                ids_started += &update_leg_as_started(cab_idx, cabs, leg);

            }
            cabs[cab_idx as usize].started = Local::now().naive_local();
        },
        BusStatus::WAITING => {
            if elapsed > stop_wait as i64 {
                ids_started += &update_leg_as_started(cab_idx, cabs, leg);
            } // else do nothing
        },    
        BusStatus::GOING => {
            if elapsed >= leg.dist as i64 * 60 {
                ids_completed += &update_leg_as_completed(cab_idx, leg);
                if is_last { // last leg
                    cabs[cab_idx as usize].status = BusStatus::UNASSIGNED;
                    r_ids_completed += &format!("{},", leg.route_id); 
                    sql += &update_cab(cab_idx, leg.to, CabStatus::FREE);
                } else {
                    cabs[cab_idx as usize].status = BusStatus::WAITING;
                    cabs[cab_idx as usize].started = Local::now().naive_local();
                    sql += &update_cab(cab_idx, leg.to, CabStatus::ASSIGNED);
                }
                cabs[cab_idx as usize].location = leg.to;
            } 
        }
    }
    if ids_started.len() > 0 {
        ids_started.pop();
        sql += &format!("UPDATE leg SET status=5, started=now() WHERE id IN ({});", ids_started); // 5: STARTED
    }
    if ids_completed.len() > 0 {
        ids_completed.pop();
        sql += &format!("UPDATE leg SET status=6, completed=now() WHERE id IN ({});", ids_completed); // 6: COMPLETED
    }
    if r_ids_completed.len() > 0 {
        r_ids_completed.pop();
        sql += &format!("UPDATE route SET status=6 WHERE id IN ({});", r_ids_completed);
    }
    if r_ids_started.len() > 0 {
        r_ids_started.pop();
        sql += &format!("UPDATE route SET status=5 WHERE id IN ({});", r_ids_started); // STARTED
    }
    return sql;
}

fn update_leg_as_started(cab_idx: i64, cabs: &mut Vec<Bus>, leg: Leg) -> String {
    cabs[cab_idx as usize].status = BusStatus::GOING;
    cabs[cab_idx as usize].started = Local::now().naive_local();
    info!("Cab cab_id={}, is moving from {} to {}, leg_id={},", cab_idx, leg.from, leg.to, leg.id);
    return format!("{},", leg.id);
}

fn update_leg_as_completed(cab_idx: i64, leg: Leg) -> String {
    info!("Cab cab_id={}, reached the stop {}, leg_id={},", cab_idx, leg.to, leg.id);
    return format!("{},", leg.id); 
}

fn update_cab(cab_idx: i64, location: i32, status: CabStatus) -> String {
    info!("Cab cab_id={}, has status {} at stand {},", cab_idx, status, location);
    return format!("UPDATE cab SET status={}, location={} WHERE id={};", status as i32, location, cab_idx); 
}

fn init_cabs(client: &mut PooledConn, max_cab: u16, max_stand: u16) -> Vec<Bus> {
    let sql: String;
    let mut cabs: Vec<Bus> = Vec::new();
    // making cabs free, two scenarios, try to spread cabs:
    if max_cab > max_stand {
        // parameters not sent as params as there was a strange error - a key should be i8
        sql = format!("UPDATE cab SET status=1, location=id % {} WHERE id < {};", max_stand, max_cab);
    } else {
        sql = format!("UPDATE cab SET status=1, location=id * ({}/{}) WHERE id < {};", max_stand, max_cab, max_cab);
    }
    client.query_iter(&sql).unwrap();

    for i in 0..max_cab {
        cabs.push(Bus { status: BusStatus::UNASSIGNED, 
                        leg: Leg { id: -1, route_id: -1, cab_id: -1, from: -1, to: -1, place: -1, dist: -1,
                                 reserve: -1, started: None, completed: None, status: RouteStatus::COMPLETED, passengers: -1 },
                        cab: i as i64,
                        started: Local::now().naive_local(),
                        location: if max_cab > max_stand { i %  max_stand } else { i * (max_stand/     max_cab) } as i32});
    }
    return cabs;
}

fn read_cfg_file(cfg_file: String) -> HashMap<String, String> {
    let settings = config::Config::builder()
        .add_source(config::File::with_name(&cfg_file))
        .build()
        .unwrap();
    return settings
        .try_deserialize::<HashMap<String, String>>()
        .unwrap();
}

fn get_cfg(cfg_src: &HashMap<String, String>) -> HashMap<String, u16> {
    let mut cfg: HashMap<String, u16> = HashMap::new();
    for v in CFG_ENTRIES {
        if v == &"db_conn" || v == &"log_file" || v == &"orders_file" { continue; }
        cfg.insert(v.to_string(), cfg_src[*v].parse().unwrap());
    }
    return cfg;
}

fn setup_logger(file_path: String) {
    let level = log::LevelFilter::Info;
    // Build a stderr logger.
    let stderr = ConsoleAppender::builder().target(Target::Stderr).build();
    // Logging to log file.
    let logfile = FileAppender::builder()
        // Pattern: https://docs.rs/log4rs/*/log4rs/encode/pattern/index.html
        .encoder(Box::new(PatternEncoder::new("{d(%Y-%m-%d %H:%M:%S)} {l} - {m}\n")))
        .build(file_path)
        .unwrap();

    // Log Trace level output to file where trace is the default level
    // and the programmatically specified level to stderr.
    let config = Config::builder()
        .appender(Appender::builder().build("logfile", Box::new(logfile)))
        .appender(
            Appender::builder()
                .filter(Box::new(ThresholdFilter::new(level)))
                .build("stderr", Box::new(stderr)),
        )
        .build(
            Root::builder()
                .appender("logfile")
                .appender("stderr")
                .build(LevelFilter::Debug),
        )
        .unwrap();

    // Use this to change log levels at runtime.
    // This means you can change the default log level to trace
    // if you are trying to debug an issue and need more logs on then turn it off
    // once you are done.
    let _handle = log4rs::init_config(config);
}
