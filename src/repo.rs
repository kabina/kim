use chrono::NaiveDateTime;
use log::warn;
use mysql::*;
use mysql::prelude::*;
use crate::model::{Stop, Order, OrderStatus, Leg, RouteStatus};

pub fn read_stops(client: &mut PooledConn) -> Vec<Stop> {
    return client.query_map("SELECT id, latitude, longitude, bearing FROM stop",
                    |(id, latitude, longitude, bearing)| {
                         Stop { id, latitude, longitude, bearing }
            },).unwrap();
}

fn get_naivedate(row: &Row, index: usize) -> Option<NaiveDateTime> {
    let val: Option<mysql::Value> = row.get(index);
    return match val {
        Some(x) => {
            if x == Value::NULL {
                None
            } else {
                row.get(index)
            }
        }
        None => None
    };
}

pub fn read_routes(c: &mut PooledConn) -> Vec<Leg> {
    let mut legs: Vec<Leg> = vec![];
    let leg_sql = "SELECT l.id, from_stand, to_stand, place, distance, started, completed, \
                        l.status, route_id, r.cab_id, passengers FROM leg l, route r \
                        WHERE route_id=r.id and (l.status=1 OR l.status=5) ORDER by route_id, place".to_string();
    let routes: Result<Vec<Row>> = c.query(leg_sql);
    match routes {
        Ok(sel) => {   
            for row in sel {
                legs.push(Leg { 
                    id:     row.get(0).unwrap(), 
                    from:   row.get(1).unwrap(), 
                    to:     row.get(2).unwrap(), 
                    place:  row.get(3).unwrap(), 
                    dist:   row.get(4).unwrap(), 
                    started:get_naivedate(&row, 5), 
                    completed:get_naivedate(&row, 6), 
                    status: get_route_status(row.get(7).unwrap()),
                    route_id: row.get(8).unwrap(),
                    cab_id: row.get(9).unwrap(),
                    passengers: row.get(10).unwrap(),
                    reserve: 0,
                });
            }
        }
        Err(err) => {
            warn!("SQL error: {}", err); // most likely deadlock
            return legs;
        }
    } 
    return legs;
}

pub fn read_orders(c: &mut  PooledConn) -> Vec<Order> {
    let mut ret: Vec<Order> = vec![];
    // is there a bug in KERN, taxi_order with status=7 er missing cab_id ?? that's why a JOIN here, get cab_id from route:
    let sql = "SELECT id, cab_id, customer_id, from_stand, to_stand, max_loss, max_wait,\
                        shared, in_pool, eta, status, received, started, completed, distance, at_time \
                        FROM taxi_order WHERE status<3 OR status=6 OR status=7".to_string();
    let orders: Result<Vec<Row>> = c.query(sql);
    match orders {
        Ok(sel) => {
            for row in sel {                        
                ret.push(Order { 
                    id:     row.get(0).unwrap(), 
                    cab_id: row.get(1).unwrap(), 
                    cust_id:row.get(2).unwrap(), 
                    from:   row.get(3).unwrap(), 
                    to:     row.get(4).unwrap(), 
                    loss:   row.get(5).unwrap(), 
                    wait:   row.get(6).unwrap(), 
                    shared: row.get(7).unwrap(), 
                    in_pool:row.get(8).unwrap(), 
                    eta:    row.get(9).unwrap(), 
                    status: get_order_status(row.get(10).unwrap()),
                    received:get_naivedate(&row, 11),
                    started:get_naivedate(&row, 12),
                    completed:get_naivedate(&row, 13),
                    dist:   row.get(14).unwrap(),
                    at_time: get_naivedate(&row, 15),
                    assigned: None
                });
            }
        }
        Err(err) => {
            warn!("SQL error: {}", err); // most likely deadlock
            return ret;
        }
    }
    return ret;
}

pub fn get_route_status(idx: i32) -> RouteStatus {
    return unsafe { ::std::mem::transmute(idx as i8) };
}

pub fn get_order_status(idx: i32) -> OrderStatus {
    return unsafe { ::std::mem::transmute(idx as i8) };
}
