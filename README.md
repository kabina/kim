# kim

In order to run Kabina client simulator make changes in *kim.toml*:
   
  | Parameter | Purpose
  |----------|--------
  | db_conn | database connection string - user, password, address, port, schema
  | log_file | both buses and passengers will be logged here
  | cab_speed | km/h, how fast does a bus drive between stops
  | max_time | duration of request submission in minutes, simulation will take longer to complete
  | check_interval | sleep duration between iterations, which submit requests and updates both buses and passengers
  | max_cab | how many buses should be emulated. there must be rows in database for them
  | max_stand | id of the last stop in the database
  | stop_wait | how long should a bus wait at a stop, in seconds
  | req_per_min | how many trips per minute should be requested
  | max_wait_for_assign  | after how many minutes of waiting for assignment should a passenger cancel a request
  |  max_delay | after what delay beyond max_wait (no bus has appeared to pick up the passenger) should a passenger cancel a request, in minutes
  |  max_trip | max duration of a trip requested, in minutes
  | max_detour | in percents, what extension of max_trip can a passenger accept in a pool (in exchange for a better price?)
  | max_trip_actual | not used
  | max_trip_delay | delay of the whole trip including detour that will cause a warning in log if exceeded, in minutes 
 
and run
   ```
   cargo run --release
   ```